module.exports = {

  networks: {
    development: {
     host: "host.docker.internal",     // Localhost (default: none)
     port: 7545,            // Standard Ethereum port (default: none)
     network_id: "*",       // Any network (default: none)
    },
  },

  // Set default mocha options here, use special reporters etc.
  mocha: {
    // timeout: 100000
  },

  // Configure your compilers
  compilers: {
    solc: {
      version: "0.8.1",    // Fetch exact version from solc-bin (default: truffle's version)

       optimizer: {
         enabled: false,
         runs: 200
       },
    }
  },

  db: {
    enabled: false
  }
};
